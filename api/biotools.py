from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqUtils import GC
import os

# Directori on es troben els fitxers fasta i genbank
GENBANK_DIR = os.path.join(os.path.dirname(__file__), 'data')

# Session 4 tools.

def get_genbank_files_list():
    genbank_files = [file for file in os.listdir(GENBANK_DIR) if file.endswith('.genbank')]
    return genbank_files

def get_genbank_info(accession):
    genbank_file = f'{GENBANK_DIR}/{accession}'
    print(genbank_file)
    try:
        records = SeqIO.parse(genbank_file, 'genbank')
        info = []
        
        for record in records:
            record_info = {
                'title': record.description,
                'accession': record.id,
                'ncbi_link': f'https://www.ncbi.nlm.nih.gov/nuccore/{record.id}',
                'latest_reference': record.annotations.get('taxonomy')[0:30],
                # 'latest_reference': record.annotations.get('references', [])[0].title,
                'num_features': len(record.features),
                'cds_info': [{'type': feature.type, 'location': str(feature.location)} for feature in record.features if feature.type == 'CDS'],
                'sequence': str(record.seq),
                'gc': round(GC(record.seq))
            }
            info.append(record_info)

        return info
    except FileNotFoundError:
        return {'error': f'No es troba cap fitxer genbank amb l\'accession {accession}. Suggerim provar amb el NC_045512; el del SarsCov2.'}
