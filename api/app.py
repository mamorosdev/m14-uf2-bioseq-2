from flask import Flask, request, jsonify, Response, json
import sequence
from Bio.Seq import Seq
from Bio.SeqUtils import seq3
import time
import biotools
from Bio.SeqUtils import GC
import os
#from flask_cors import CORS

app = Flask(__name__, static_folder="out", static_url_path="/")
#cors = CORS(app, resources={r"/translate": {"origins": "http://localhost:3000"}})
GENBANK_DIR = os.path.join(os.path.dirname(__file__), 'data')

@app.route("/")
def index():
    return app.send_static_file("index.html")

@app.route("/api/time")
def get_current_time():
    return {"time": time.time()}

## Web Services Sessió 2.

def _translate_dna(dna_sequence: str, translation_table: int = 1):
    mrna_seq = Seq(dna_sequence)
    protein_seq = mrna_seq.translate(table=translation_table)
    return protein_seq

def _analyze_cds(protein_seq):
    start_codon = "M"
    stop_codons = ["*", "Ter"]
    cds_segments = []
    incomplete_proteins = []

    for start_index in range(0, len(protein_seq)):
        amino_acid = protein_seq[start_index]
        if amino_acid == start_codon:
            cds = protein_seq[start_index:]
            for stop_codon in stop_codons:
                stop_index = cds.find(stop_codon)
                if stop_index != -1:
                    cds_segments.append(str(cds[:stop_index + 1]))
                    break
            else:
                incomplete_proteins.append(str(cds))

    return cds_segments, incomplete_proteins

@app.route('/api/translate', methods=['POST'])
def translate():
    data = request.get_json()

    if 'dna_seq' in data:
        dna_seq = data['dna_seq']
        print(dna_seq)
        translation_table = data.get('translation_table', 1)

        seq_adn_valid = all(base in "ACGTacgt" for base in dna_seq)

        if seq_adn_valid:
            seq_arn = Seq(dna_seq).transcribe()
            protein_seq = _translate_dna(dna_seq, translation_table)
            cds_segments, incomplete_proteins = _analyze_cds(protein_seq)

            result = {
                'success': True,
                'seq_arn': str(seq_arn),
                'protein_sequence': str(protein_seq),
                'cds_segments': cds_segments,
                'incomplete_proteins': incomplete_proteins,
                'gc': round(GC(dna_seq))
            }
        else:
            result = {"success": False, "error": "La seqüència d'ADN no és vàlida."}
    else:
        result = {"success": False, "error": "La seqüència d'ADN no s'ha proporcionat."}

    json_string = json.dumps(result, ensure_ascii=False)
    response = Response(json_string, content_type="application/json; charset=utf-8")
    # Activar CORS si és necessari
    # response.headers.add('Access-Control-Allow-Origin', 'http://localhost:3000')
    # response.headers.add('Access-Control-Allow-Headers', 'Content-Type')

    return response



@app.route('/genbank/<accession>', methods=['GET'])
def get_genbank(accession):
    info = biotools.get_genbank_info(accession)
    return jsonify(info)

@app.route('/api/genbank-list', methods=['GET'])
def get_genbank_list():
    genbank_files = biotools.get_genbank_files_list()
    genbank_info_list = []
    # print(genbank_files)
    for accession in genbank_files:
        genbank_info_list.append(biotools.get_genbank_info(accession))

    return jsonify(genbank_info_list)


## Web Services necessaris per arrencar la aplicació.

if __name__ == "__main__":
    app.run()