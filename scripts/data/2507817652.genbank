LOCUS       OQ672434                 145 bp    DNA     linear   INV 24-MAY-2023
DEFINITION  Plasmodium falciparum isolate CZS_17 chloroquine resistance
            transporter (crt) gene, partial cds.
ACCESSION   OQ672434
VERSION     OQ672434.1
KEYWORDS    .
SOURCE      Plasmodium falciparum (malaria parasite P. falciparum)
  ORGANISM  Plasmodium falciparum
            Eukaryota; Sar; Alveolata; Apicomplexa; Aconoidasida; Haemosporida;
            Plasmodiidae; Plasmodium; Plasmodium (Laverania).
REFERENCE   1  (bases 1 to 145)
  AUTHORS   de Abreu-Fernandes,R., Almeida-de-Oliveira,N.K., Gama,B.E.,
            Gomes,L.R., De Lavigne Mello,A.R., Queiroz,L.T., Barros,J.A.,
            Alecrim,M.G.C., Medeiros de Souza,R., Pratt-Riccio,L.R., Brasil,P.,
            Daniel-Ribeiro,C.T. and Ferreira-da-Cruz,M.F.
  TITLE     Plasmodium falciparum Chloroquine-pfcrt Resistant Haplotypes in
            Brazilian Endemic Areas Four Decades after CQ Withdrawn
  JOURNAL   Pathogens 12 (5), 731 (2023)
  REMARK    DOI: 10.3390/pathogens12050731
REFERENCE   2  (bases 1 to 145)
  AUTHORS   Cruz,M.F.F. and Santos,R.A.F.
  TITLE     Direct Submission
  JOURNAL   Submitted (16-MAR-2023) Oswaldo Cruz Institute, FIOCRUZ, Avenida
            Brasil, 4365 - Manguinhos, Rio de Janeiro, Rio de Janeiro 21040-360,
            Brasil
COMMENT     ##Assembly-Data-START##
            Sequencing Technology :: Sanger dideoxy sequencing
            ##Assembly-Data-END##
FEATURES             Location/Qualifiers
     source          1..145
                     /organism="Plasmodium falciparum"
                     /mol_type="genomic DNA"
                     /isolate="CZS_17"
                     /db_xref="taxon:5833"
                     /country="Brazil"
     gene            <1..>145
                     /gene="crt"
                     /note="Pfcrt"
     mRNA            <1..>145
                     /gene="crt"
                     /product="chloroquine resistance transporter"
     CDS             <1..>145
                     /gene="crt"
                     /note="localized within the digestive vacuole membrane"
                     /codon_start=1
                     /product="chloroquine resistance transporter"
                     /protein_id="WHO19583.1"
                     /translation="CAHVFKLIFKEIKDNIFIYILSIIYLSVSVMNTIFAKRTLNKIGN
                     YSF"
ORIGIN
        1 tgtgctcatg tgtttaaact tatttttaaa gagattaagg ataatatttt tatttatatt
       61 ttaagtatta tttatttaag tgtatctgta atgaatacaa tttttgctaa aagaacttta
      121 aacaaaattg gtaactatag ttttg
//
