from Bio import Entrez, SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

import os

# gb_accession_prefix = 'AY' # First genbank is AY156734
# gb_accession_ids = list(range(156734, 156764, 1))
save_folder = "./data/"
Entrez.email = "mamoro10@xtec.cat"

def read_accessionIds():
    print("Step 1. ePost Accession ID's.")
    # Convert id numbers strings y agregar el prefijo
    # accession_ids_str = [f"{gb_accession_prefix}{id}" for id in gb_accession_ids]

    handle = Entrez.esearch(db="nucleotide", term='CRT[Gene Name] AND "Plasmodium falciparum"[Organism]', retmax="30")
    rec_list = Entrez.read(handle)
    handle.close()
    # print(rec_list['IdList'])
    return rec_list['IdList']

def download_genbanks_fastas(accession_list: list[str]):
    # search_handle = Entrez.epost(db="nucleotide", id=",".join(accession_ids_str))
    # EPost necessari perquè demanem molts fitxers a la vegada.
    search_handle = Entrez.epost(db="nucleotide", id=accession_list)
    search_results = Entrez.read(search_handle)
    webenv = search_results["WebEnv"]
    query_key = search_results["QueryKey"]

    print("Step 2. eFetch sequences info.")
    handle = Entrez.efetch(db="nucleotide", webenv=webenv, query_key=query_key, rettype="gb", retmode="text")
    genbank_records = list(SeqIO.parse(handle, "genbank"))

    for i, genbank_record in enumerate(genbank_records):
        filename_genbank = os.path.join(save_folder, f"{accession_list[i]}.genbank")
        filename_fasta = os.path.join(save_folder, f"{accession_list[i]}.fasta")
        # print(filename)
        if not os.path.exists(filename_genbank):
            SeqIO.write(genbank_record, filename_genbank, "genbank")

        # Per estalviarnos descarregar fastas podem fer aquest truc (tot i que la capçalera no serà exacte).
        # Accedeix a la seqüència i converteix-la a Seq si no és ja una Seq
        sequence = genbank_record.seq
        fasta_record = SeqRecord(Seq(sequence), id=genbank_record.id, name=genbank_record.name, description=genbank_record.description)
        if not os.path.exists(filename_fasta):
            SeqIO.write(fasta_record,  filename_fasta, "fasta")

    print('Genbank and fasta files downloaded in ', save_folder)  

# -----------------------------------------------------------------------------
def main():
    
    accession_list: list[str] =  read_accessionIds()
    download_genbanks_fastas(accession_list)

# Main
# -----------------------------------------------------------------------------
this_module = __name__
main_module = '__main__'

if (this_module == main_module): main()
# -----------------------------------------------------------------------------


