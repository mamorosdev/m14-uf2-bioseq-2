'use client';
import { useState } from 'react';

export default function Home6() {

  const [protid, setProtid] = useState<string>("4N7N"); // Proteïna ID per defecte

  const handleLoad = () => {
    const inputElement = document.getElementById("name") as HTMLInputElement;
    setProtid(inputElement.value);
  }

  const url = `https://www.ncbi.nlm.nih.gov/Structure/icn3d/?mmdbid=${protid}&closepopup=1&showcommand=1&shownote=1&mobilemenu=1&showtitle=1`;

  return (
    <>
      <main className="container mt-5">
        <h2 className="text-center mt-5 fs-2">BioActivitat 6 - Estructures de proteïnes.</h2>
        <div className="flex items-center space-x-4 mt-4">
          <input id="name" type="text" placeholder="4N7N" className="border border-indigo-500 px-4 py-2 rounded focus:outline-none" defaultValue={protid} />

          <button onClick={handleLoad} className="bg-indigo-500 text-white py-2 px-4 rounded focus:outline-none hover:bg-indigo-600">Load Protein</button>
        </div>

        <iframe src={url} className="w-full mt-4 aspect-video"></iframe>
        
        <p className="mt-4">Font: <a href="https://www.ncbi.nlm.nih.gov/Structure/icn3d" className="text-indigo-500">https://www.ncbi.nlm.nih.gov/Structure/icn3d</a></p>
      </main>
    </>
  );
}